package brx.tools.wls.jms;
/*
 * Copyright 2007 Sun Microsystems, Inc.
 * All rights reserved.  You may not modify, use,
 * reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * http://developer.sun.com/berkeley_license.html
 */

import java.util.Hashtable;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Connection;
import javax.jms.Session;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * The Producer class consists only of a main method, which sends several
 * messages to a queue or topic.
 *
 * Run this program in conjunction with SynchConsumer or AsynchConsumer. Specify
 * "queue" or "topic" on the command line when you run the program. By default,
 * the program sends one message. Specify a number after the destination name to
 * send that number of messages.
 */
public class Producer {

	static String USAGE = "Usage: Producer PROVIDER_URL CONNECTION_FACTORY QUEUE NUM_OF_MESSAGES";
	static String SLEEPString = System.getProperty("SLEEP");
	static int SLEEP = 0;

	public Context refreshContext(Hashtable<?, ?> env) {
		Context jndiContext = null;
		try {
			jndiContext = new InitialContext(env);
		} catch (NamingException e) {
			System.err.println("Could not create JNDI API " + "context: " + e.toString());
			return null;
		}
		System.out.println("Refreshed Context: " + jndiContext.toString());
		return jndiContext;
	}

	public ConnectionFactory refreshConnectionFactory(Context jndiContext, String connectionFactoryName) {
		/*
		 * Look up connection factory and queue. If either does not exist, exit.
		 */
		ConnectionFactory connectionFactory = null;
		try {
			connectionFactory = (ConnectionFactory) jndiContext.lookup(connectionFactoryName);
		} catch (NamingException e) {
			System.err.println("Connection Factory lookup failed: " + e.toString());
			return null;
		}
		return connectionFactory;
	}

	public Destination refreshDestination(Context jndiContext, String destName) {
		/*
		 * Look up connection factory and queue. If either does not exist, exit.
		 */
		Destination dest = null;
		try {
			dest = (Destination) jndiContext.lookup(destName);
		} catch (NamingException e) {
			System.err.println("Destination lookup failed: " + e.toString());
			return null;
		}
		return dest;
	}

	public StringBuffer genMessages(String providerUrl, String connectionFactoryName, String destName, long numMess) {

		StringBuffer sbout = new StringBuffer();

		sbout.append("PROVIDER_URL: '" + providerUrl + "'.\n");
		sbout.append("CONNECTION_FACTORY: '" + connectionFactoryName + "'.\n");
		sbout.append("DESTINATION: '" + destName + "'.\n");
		
		System.out.print(sbout);
		
		/* Setup Environment */
		Hashtable<String, String> env = new Hashtable<String, String>();
		// env.put(Context.INITIAL_CONTEXT_FACTORY,
		// "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		env.put(Context.PROVIDER_URL, providerUrl);
		// env.put(Context.SECURITY_PRINCIPAL, "weblogic");
		// env.put(Context.SECURITY_CREDENTIALS, "welcome1");
		/*
		 * Create a JNDI API InitialContext object if none exists yet.
		 */

		Context jndiContext = refreshContext(env);
		if (jndiContext == null) {
			throw new RuntimeException("jndiContext is null.");
		}
		sbout.append("JNDI Context created.");
		/*
		 * Create connection. Create session from connection; false means
		 * session is not transacted. Create producer and text message. Send
		 * messages, varying text slightly. Send end-of-messages message.
		 * Finally, close connection.
		 */
		ConnectionFactory connectionFactory = refreshConnectionFactory(jndiContext, connectionFactoryName);
		sbout.append("Connection factory acquired.");
		Destination dest = refreshDestination(jndiContext, destName);
		sbout.append("Destination acquired.");
		Connection connection = null;

		try {
			connection = connectionFactory.createConnection();
			System.out.println(connection);
			sbout.append(connection);

			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			System.out.println(session);
			sbout.append(session);

			MessageProducer producer = session.createProducer(dest);
			System.out.println(producer);
			sbout.append(producer);

			TextMessage message = session.createTextMessage();
			System.out.println(message);
			sbout.append(message);

			System.out.println("Destination: " + producer.getDestination().toString());
			sbout.append("Destination: " + producer.getDestination().toString());

			long startTimestamp = System.currentTimeMillis();

			for (int i = 0; i < numMess; i++) {
				message.setText("Message " + i);
				if (numMess < 1000) {
					System.out.println("Sending message: " + (i + 1));
					sbout.append("Sending message: " + (i + 1));
				} else {
					// System.out.print(".");
					if (i % 1000 == 1) {
						long currentTimestamp = System.currentTimeMillis();
						long deltaTimestamp = currentTimestamp - startTimestamp;
						System.out.println(deltaTimestamp + " - Sending message: " + i);
					}
				}
				producer.send(message);
				try {
					Thread.sleep(SLEEP);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			/*
			 * Send a non-text control message indicating end of messages.
			 */
			producer.send(session.createMessage());
		} catch (JMSException e) {
			System.err.println("Exception occurred: " + e.toString());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
				}
			}
		}
		return sbout;
	}

	public static void main(String[] args) {

		if (SLEEPString == null) {
			SLEEP = 2000;
		} else {
			SLEEP = new Integer(SLEEPString);
		}
		if (args.length < 4) {
			System.err.println(USAGE);
			System.exit(1);
		}

		String providerUrl = args[0];
		String connectionFactoryName = args[1];
		String destName = args[2];
		final long numMess;
		if (args.length == 4) {
			numMess = (new Long(args[3])).longValue();
		} else {
			numMess = 1;
		}
		
		Producer producer = new Producer();
		try{
		producer.genMessages(providerUrl, connectionFactoryName, destName, numMess);
		}catch (Exception e){
			e.printStackTrace();
			System.exit(1);
		}

	}
}
