package brx.tools.wls.jms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ProducerServlet
 */
@WebServlet("/ProducerServlet")
public class ProducerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProducerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String providerUrl = request.getParameter("providerUrl");
		String connectionFactoryName = request.getParameter("connectionFactoryName");
		String destName = request.getParameter("destName");
		final long numMess = (new Long(request.getParameter("numMess"))).longValue();
		
		Producer producer = new Producer();
		StringBuffer out = producer.genMessages(providerUrl, connectionFactoryName, destName, numMess);
		PrintWriter writer = response.getWriter();
        writer.print(out);
	}

}
