package brx.tools.wls.jndi;
/*
 * Copyright 2007 Sun Microsystems, Inc.
 * All rights reserved.  You may not modify, use,
 * reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * http://developer.sun.com/berkeley_license.html
 */

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * The Producer class consists only of a main method, which sends several
 * messages to a queue or topic.
 *
 * Run this program in conjunction with SynchConsumer or AsynchConsumer. Specify
 * "queue" or "topic" on the command line when you run the program. By default,
 * the program sends one message. Specify a number after the destination name to
 * send that number of messages.
 */
public class Lookup {

    static String USAGE = "Usage: Lookup PROVIDER_URL USER PASSWORD NAME_TO_LOOKUP";

    public static Context refreshContext(Hashtable<?, ?> env) {
        Context context = null;
        try {
            context = new InitialContext(env);
        } catch (NamingException e) {
            System.err.println("Could not create JNDI API " + "context: " + e.toString());
            return null;
        }
        System.out.println("Refreshed Context: " + context.toString());
        return context;
    }

    public static void main(String[] args) {
        String SLEEPString = System.getProperty("SLEEP");
        int SLEEP = 0;

        if (SLEEPString == null) {
            SLEEP = 2000;
        } else {
            SLEEP = new Integer(SLEEPString);
        }
        if (args.length < 4) {
            System.err.println(USAGE);
            System.exit(1);
        }

        String providerUrl = args[0];
        String user = args[1];
        String password = args[2];
        String name = args[3];

        System.out.println("PROVIDER_URL: '" + providerUrl + "'.");
        System.out.println("SCURITY_PRINCIPAL: '" + user + "'.");
        System.out.println("NAMT to LOOKUP: '" + name + "'.");

        /* Setup Environment */
        Hashtable<String, String> env = new Hashtable<String, String>();
        // env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL, providerUrl);
        env.put(Context.SECURITY_PRINCIPAL, user);
        env.put(Context.SECURITY_CREDENTIALS, password);
        /*
         * Create a JNDI API InitialContext object if none exists yet.
         */

        Context context = refreshContext(env);
        if (context == null) {
            System.err.println("ERROR: Context is NULL.");
            System.exit(1);
        }
        Object o = null;
        try {
            o = context.lookup(name);
        } catch (NamingException e) {
            System.err.println("Object lookup failed: " + e.toString());
            System.exit(1);
        }
        ;
        if (o == null) {
            System.out.println("Lookup returned null object.");
        } else {
            System.out.println("Found object: " + o);
        }
    }
}
