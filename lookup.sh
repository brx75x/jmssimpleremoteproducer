#!/usr/bin/env bash
JARFILE="target/JMSSimpleRemoteProducer-0.0.1-SNAPSHOT.jar"
MAVEN_CP="$(mvn  -B dependency:build-classpath | grep -v '^\[')"
CLASS="brx.tools.wls.jndi.Lookup"

java $JAVA_OPTIONS -Djava.util.logging.config.file=logging.properties  -cp $MAVEN_CP:$JARFILE $CLASS $@
