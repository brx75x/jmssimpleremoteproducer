#!/usr/bin/env bash
TESTS='\
t3://localhost:17001,17002 jms/ConnectionFactory-0 jms/DistributedQueue-0 5000;
'
N_TESTS="$(echo $TESTS| fgrep -o ';'| wc -l )"

JARFILE="target/JMSSimpleRemoteProducer-0.0.1-SNAPSHOT.jar"
MAVEN_CP="$(mvn  -B dependency:build-classpath | grep -v '^\[')"
CLASS="brx.tools.wls.jms.HAProducer"
JAVA_OPTIONS="-DSLEEP=500"

i=1
echo "$TESTS" | tr ';' '\n' | while read; do
    test -z "$REPLY" && continue
    echo "### $i..$N_TESTS - $REPLY"
    java $JAVA_OPTIONS -Djava.util.logging.config.file=logging.properties  -cp $MAVEN_CP:$JARFILE $CLASS $REPLY
    RC="$?"
    echo "### RC $RC"
    if [[ $RC -ne 0 ]]; then
        exit
    fi
    i=$(( i + 1 ))
done
